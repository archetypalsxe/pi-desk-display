function setTime()
{
  weekdays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  let date = new Date();
  $("#date").html(weekdays[date.getDay()] +" "+ date.toLocaleDateString());
  $("#time").html(date.toLocaleTimeString());
}

function rotate(result)
{
  if (navigator.browser != 'Chrome')
    return false;

  if (navigator.version > 80)
    return false;

  return result.rotate;
}

function getPhoto()
{
  $.ajax({url: "server/get_images.php", success: function(result){
    console.log("Result: "+result);
    result_object = JSON.parse(result);
    $("#image").attr("src",result_object.location);
    $('#image').css({'transform': 'rotate(0deg)'});
    if (rotate(result_object)) {
      console.log("Rotating...");
      $('#image').css({'transform': 'rotate(270deg)'});
    } else {
      console.log("Not rotating...");
    }
  }});
}

navigator.browser= (function(){
    var ua= navigator.userAgent, tem,
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return M[0];
})();

navigator.version= (function(){
    var ua= navigator.userAgent, tem,
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return M[1];
})();

$(function() {
  $("#entire-page").on("swiperight", swipe);
  $("#entire-page").on("swipeleft", swipe);

  function swipe(event) {
    getPhoto();
  }
});

document.onkeydown = function(e) {
    switch(e.which) {
        case 37: // left
        getPhoto();
        break;

        case 38: // up
        getPhoto();
        break;

        case 39: // right
        getPhoto();
        break;

        case 40: // down
        getPhoto();
        break;

        default: return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
};

setTime();
getPhoto();
setInterval(setTime, 1000);
setInterval(getPhoto, 10000);
