<?php

$files = array_diff(scandir('../images'), array('..', '.', '.keep'));
$image_num = mt_rand(0, sizeof($files) - 1);
$image = array_slice($files, $image_num, 1);

$image_name = $image[0];

$rotate = strpos($image_name, "ROTATE") === 0;
$image_location = "images/$image_name";

echo json_encode([
  'location' => $image_location,
  'rotate' => $rotate]);
