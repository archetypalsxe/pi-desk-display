# pi-desk-display

Code for a project involving a Raspberry Pi powered desk display. Going to display things like pictures along with time/date. Perhaps eventually a to-do list and temperature

## Getting Going on a Pi
### Dependencies
* chromium (can use whatever browser, just have to modify the startup script)
* docker (and daemon running)
* docker-compose
* zip
* Automatically connects to the internet

### Running on Boot
* If the file `~/.config/lxsession/LXDE-pi/autostart` does not exist we need to create it...
  * `cp /etc/xdg/lxsession/LXDE-pi/autostart ~/.config/lxsession/LXDE-pi/`
* At the end of the autostart file created previously add either:
  * `@~/startup.sh`
  * or...
  * `@lxterminal --command ~/startup.sh`
    * Opens up a terminal window so you can see the output for debugging/troubleshooting
* Upload the `startup.sh` to the home directory
* Run the `startup.sh` file automatically

## Instructions for Running (Locally)
### With Docker Compose
More for "development" where web files are kept up to date on the container
#### Build and Run
* `docker-compose up --build`

### With Docker
More for "production" where web files being in sync on container are not necessary
#### Build
* `docker build -t nginx-container .`
#### Run
* `docker run --name nginx-container -d -p 8080:80 nginx-container`
  * It should be available at `localhost:8080` from a web browser
