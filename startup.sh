#!/bin/bash

sleep 4s

cd /tmp &&
  rm -rf display &&
  mkdir -p display &&
  cd display &&
  wget https://gitlab.com/archetypalsxe/pi-desk-display/-/archive/master/pi-desk-display-master.zip &&
  unzip pi-desk-display-master.zip &&
  cd pi-desk-display-master &&
  cp startup.sh ~/ && # Update to the newest startup script
  cp ~/Pictures/* html/images/ && # Get some photos into the Docker image
  sudo xset s off &&
  sudo xset -dpms &&
  sudo xset s noblank &&
  docker-compose up --build -d &&
  chromium-browser --incognito --start-fullscreen http://localhost:8080

cd -
